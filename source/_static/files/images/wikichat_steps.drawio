<mxfile host="app.diagrams.net" modified="2024-02-22T09:59:46.891Z" agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:123.0) Gecko/20100101 Firefox/123.0" etag="yE9-8Q-_y_y7Ioof1L1f" version="23.1.1" type="device">
  <diagram name="Page-1" id="bTky45zc6vRA1HYnYF_T">
    <mxGraphModel dx="2150" dy="768" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169" math="0" shadow="0">
      <root>
        <mxCell id="0" />
        <mxCell id="1" parent="0" />
        <mxCell id="urUjhuiAObpK9QewNi63-1" value="Step 1: Retrieve pertinent documents" style="rounded=1;whiteSpace=wrap;html=1;horizontal=1;verticalAlign=bottom;labelPosition=center;verticalLabelPosition=top;align=left;fontStyle=1;fontSize=16;fillColor=#f5f5f5;fontColor=#333333;strokeColor=#666666;" vertex="1" parent="1">
          <mxGeometry x="230" y="140" width="440" height="60" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-7" value="&lt;font style=&quot;font-size: 20px;&quot;&gt;search_query = &lt;font color=&quot;#007fff&quot;&gt;&lt;b&gt;LLM &lt;/b&gt;&lt;/font&gt;(&lt;font color=&quot;#a680b8&quot;&gt;&lt;b&gt;prompt, &lt;/b&gt;&lt;/font&gt;informed_time)&lt;br&gt;&lt;b&gt;&lt;font color=&quot;#ea6b66&quot;&gt;articles &lt;/font&gt;&lt;/b&gt;= &lt;b&gt;&lt;font color=&quot;#ff9933&quot;&gt;IR&lt;/font&gt;&lt;/b&gt;(search_query)&lt;/font&gt;" style="text;html=1;strokeColor=none;fillColor=none;align=center;verticalAlign=middle;whiteSpace=wrap;rounded=0;" vertex="1" parent="1">
          <mxGeometry x="235" y="157" width="450" height="30" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-10" value="" style="group" vertex="1" connectable="0" parent="1">
          <mxGeometry x="-110" y="157" width="306.92" height="300" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-8" value="User : What can you tell me about Pierre Clastres ?&lt;br&gt;LLM: Pierre Clastres was a French anthropologist, best known for his studies of indigenous South American societies.&lt;br&gt;&lt;br&gt;User: What is he known for ? &lt;br&gt;&lt;br&gt;LLM: He challenged conventional interpretations of power and political organization in these societies, arguing that they did not conform to European models of chiefdoms or states.&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#b5739d&quot;&gt;&lt;b&gt;User : What are his main works ? &lt;/b&gt;&lt;br&gt;&lt;/font&gt;" style="whiteSpace=wrap;html=1;aspect=fixed;fillColor=#d5e8d4;strokeColor=#82b366;" vertex="1" parent="urUjhuiAObpK9QewNi63-10">
          <mxGeometry x="36.92" y="30" width="270" height="270" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-9" value="&lt;b&gt;&lt;font style=&quot;font-size: 16px;&quot;&gt;Chat history&lt;/font&gt;&lt;/b&gt;" style="text;html=1;strokeColor=none;fillColor=none;align=center;verticalAlign=middle;whiteSpace=wrap;rounded=0;fontColor=#00994D;" vertex="1" parent="urUjhuiAObpK9QewNi63-10">
          <mxGeometry width="173.08" height="30" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-12" value="" style="group" vertex="1" connectable="0" parent="1">
          <mxGeometry x="220" y="220" width="450" height="50" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-6" value="Step 2: Extract relevant passages from articles " style="rounded=1;whiteSpace=wrap;html=1;horizontal=1;verticalAlign=bottom;labelPosition=center;verticalLabelPosition=top;align=left;fontStyle=1;fontSize=16;fillColor=#f5f5f5;fontColor=#333333;strokeColor=#666666;" vertex="1" parent="urUjhuiAObpK9QewNi63-12">
          <mxGeometry x="10" y="10" width="440" height="40" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-3" value="&lt;font style=&quot;font-size: 20px;&quot;&gt;passages = &lt;font color=&quot;#007fff&quot;&gt;&lt;b&gt;LLM &lt;/b&gt;&lt;/font&gt;(search_query, &lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;&lt;font color=&quot;#ea6b66&quot;&gt;&lt;b&gt;articles&lt;/b&gt;&lt;/font&gt;)&lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;&lt;br&gt;&lt;/font&gt;" style="text;html=1;strokeColor=none;fillColor=none;align=center;verticalAlign=middle;whiteSpace=wrap;rounded=0;" vertex="1" parent="urUjhuiAObpK9QewNi63-12">
          <mxGeometry y="16.666666666666668" width="450" height="25" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-13" value="Step 3: Response to history" style="rounded=1;whiteSpace=wrap;html=1;horizontal=1;verticalAlign=bottom;labelPosition=center;verticalLabelPosition=top;align=left;fontStyle=1;fontSize=16;fillColor=#f5f5f5;fontColor=#333333;strokeColor=#666666;" vertex="1" parent="1">
          <mxGeometry x="230" y="300" width="440" height="40" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-14" value="Step 4: Extract claims and associate evidences" style="rounded=1;whiteSpace=wrap;html=1;horizontal=1;verticalAlign=bottom;labelPosition=center;verticalLabelPosition=top;align=left;fontStyle=1;fontSize=16;fillColor=#f5f5f5;fontColor=#333333;strokeColor=#666666;" vertex="1" parent="1">
          <mxGeometry x="230" y="370" width="440" height="60" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-15" value="&lt;font style=&quot;font-size: 20px;&quot;&gt;history_response = &lt;font color=&quot;#007fff&quot;&gt;&lt;b&gt;LLM &lt;/b&gt;&lt;/font&gt;(&lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;&lt;b&gt;&lt;font color=&quot;#00994d&quot;&gt;chat history&lt;/font&gt;&lt;/b&gt;)&lt;b&gt;&lt;br&gt;&lt;/b&gt;&lt;/font&gt;" style="text;html=1;strokeColor=none;fillColor=none;align=center;verticalAlign=middle;whiteSpace=wrap;rounded=0;" vertex="1" parent="1">
          <mxGeometry x="225" y="305" width="450" height="30" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-16" value="&lt;font style=&quot;font-size: 20px;&quot;&gt;claims = &lt;font color=&quot;#007fff&quot;&gt;&lt;b&gt;LLM &lt;/b&gt;&lt;/font&gt;(history_response)&lt;br&gt;&lt;b&gt;&lt;font color=&quot;#ea6b66&quot;&gt;evidences&lt;/font&gt; &lt;/b&gt;= &lt;b&gt;&lt;font color=&quot;#ff9933&quot;&gt;IR&lt;/font&gt;&lt;/b&gt;(claims)&lt;/font&gt;" style="text;html=1;strokeColor=none;fillColor=none;align=center;verticalAlign=middle;whiteSpace=wrap;rounded=0;" vertex="1" parent="1">
          <mxGeometry x="265" y="380" width="340" height="40" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-17" value="Step 5: Fact check the claims with evidences and chain-of-prompts" style="rounded=1;whiteSpace=wrap;html=1;horizontal=1;verticalAlign=bottom;labelPosition=center;verticalLabelPosition=top;align=left;fontStyle=1;fontSize=16;fillColor=#f5f5f5;fontColor=#333333;strokeColor=#666666;" vertex="1" parent="1">
          <mxGeometry x="225" y="457" width="525" height="85" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-18" value="&lt;font style=&quot;font-size: 20px;&quot;&gt;claim_status = &lt;font color=&quot;#007fff&quot;&gt;&lt;b&gt;LLM &lt;/b&gt;&lt;/font&gt;(&lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;&lt;b&gt;&lt;font color=&quot;#00994d&quot;&gt;chat history&lt;/font&gt;&lt;/b&gt;&lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;, &lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;&lt;font color=&quot;#ea6b66&quot;&gt;&lt;b&gt;evidences&lt;/b&gt;&lt;/font&gt;, history_response, &lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;cop=True)&lt;br&gt;relevant_claims = claims[claim_status = &quot;relevant&quot;]&lt;br&gt;&lt;/font&gt;" style="text;html=1;strokeColor=none;fillColor=none;align=center;verticalAlign=middle;whiteSpace=wrap;rounded=0;" vertex="1" parent="1">
          <mxGeometry x="210" y="467" width="570" height="60" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-19" value="Step 6: Generate an answer with the relevant claims" style="rounded=1;whiteSpace=wrap;html=1;horizontal=1;verticalAlign=bottom;labelPosition=center;verticalLabelPosition=top;align=left;fontStyle=1;fontSize=16;fillColor=#f5f5f5;fontColor=#333333;strokeColor=#666666;" vertex="1" parent="1">
          <mxGeometry x="228.08" y="570" width="525" height="40" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-20" value="&lt;font style=&quot;font-size: 20px;&quot;&gt;answer = &lt;font color=&quot;#007fff&quot;&gt;&lt;b&gt;LLM &lt;/b&gt;&lt;/font&gt;(&lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;&lt;b&gt;&lt;font color=&quot;#00994d&quot;&gt;chat history&lt;/font&gt;&lt;/b&gt;&lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;, &lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;relevant_claims&lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;)&lt;br&gt;&lt;/font&gt;" style="text;html=1;strokeColor=none;fillColor=none;align=center;verticalAlign=middle;whiteSpace=wrap;rounded=0;" vertex="1" parent="1">
          <mxGeometry x="210" y="575" width="570" height="30" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-21" value="Step 7: Refine the answer" style="rounded=1;whiteSpace=wrap;html=1;horizontal=1;verticalAlign=bottom;labelPosition=center;verticalLabelPosition=top;align=left;fontStyle=1;fontSize=16;fillColor=#f5f5f5;fontColor=#333333;strokeColor=#666666;" vertex="1" parent="1">
          <mxGeometry x="225" y="640" width="525" height="40" as="geometry" />
        </mxCell>
        <mxCell id="urUjhuiAObpK9QewNi63-22" value="&lt;font style=&quot;font-size: 20px;&quot;&gt;final answer = &lt;font color=&quot;#007fff&quot;&gt;&lt;b&gt;LLM &lt;/b&gt;&lt;/font&gt;(&lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;&lt;b&gt;&lt;font color=&quot;#00994d&quot;&gt;chat history&lt;/font&gt;&lt;/b&gt;&lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;, &lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;answer, cop=True&lt;/font&gt;&lt;font style=&quot;font-size: 20px;&quot;&gt;)&lt;br&gt;&lt;/font&gt;" style="text;html=1;strokeColor=none;fillColor=none;align=center;verticalAlign=middle;whiteSpace=wrap;rounded=0;" vertex="1" parent="1">
          <mxGeometry x="210" y="650" width="570" height="30" as="geometry" />
        </mxCell>
      </root>
    </mxGraphModel>
  </diagram>
</mxfile>
