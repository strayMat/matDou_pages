# 🛠 Toolbox

Echanging with collegues, I come accross a lot of data science tools and packages. 

🤩 Some are very intersting and are essential building blocks for my projects. 

😗 Others stays here for an undefined amount of time waiting to be tested.

## Data-preprocessing

- 😗[pandas-profiling](https://pandas-profiling.ydata.ai/docs/master/): Extension of the basic `pd.describe`. Troubles to pip install (conflict with matplotlib).

- 😗 [pygwalker](https://github.com/Kanaries/pygwalker): Datavizaulization with
  quick Drag and drop manipulation on pandas.

**Big data on small laptops :** During the 2022 SoDa retreat, [Olivier Grisel](https://mobile.twitter.com/ogrisel) tested the [ibis on duckdb combo](https://gitlab.inria.fr/soda/retreat_2022_kkbox_churn/-/blob/main/prepare_duckdb.py) on the kkbox dataset (400m user logs, loaded in 15min from csv into the db). Very impressive for simple analytical operations: 2 seconds for `groupby.mean`. 

- 🤩[duckdb](https://duckdb.org/docs/guides/python/ibis) : easy to use (ie. serveless, in-process) database engine, focused on medium-sized data and analytical querries (eg. join, groupby). To be used with ibis for python API.

- 🤩[ibis](https://ibis-project.org/docs/): Querry any SQL engine with python API.

- 🤩 [schemaspy](https://schemaspy.org/) : Connect to a database and build an interactive table for schema exploration.

- 🤩 [polars](https://www.pola.rs/): Rust implementation of pandas with query optimizer and lazy loading. Ultra fast for medium-sized datasets (100M x 20). 

## Documentation 

- 🤩 [sphinx](https://www.sphinx-doc.org/en/master/): Technical documentation with python, based on markdown and restructured text. For the front, the [pydata sphinx theme](https://pydata-sphinx-theme.readthedocs.io/en/stable/index.html) is a good choice. This website is built with this combo.  

- 🤩 [diataxis](https://diataxis.fr/): high level principles for technical documentations. 


## Epidemiology 

### Survival analysis

- 🤩[lifelines](https://lifelines.readthedocs.io) : Simple models and tools for survival analysis. Good API documentation for quick usage. Not a lot of explanation on model evaluation.

- 🤩[scikit-survival](https://scikit-survival.readthedocs.io/) : Good introductory notebooks and a didactic doc on model evaluation with references. 

Rq: In the 2022 SoDa retreat, we found out that there is no survival forest implmentation scaling to big sample size (over 20,000). The cause are repeated NlogN `qsort` called in the tree splitting criterion ([logrank](https://en.wikipedia.org/wiki/Logrank_test)).  

## Causal Inference

- [Uplift-analysis](https://playtikaresearch.github.io/uplift-analysis/build/html/tutorials.html): python package for uplift analysis, a set of method for individualized treatment analysis. The term *uplift modeling* is used in marketing of financial applications. In healthcare, it corresponds to *personnalized medecine*. 

## Coding

- [Practical exercices for
  python](https://github.com/donnemartin/interactive-coding-challenges)
