# Looking into environmental sustainibility of healthcare

## Notes on [The Environmental Sustainability of Health Care Systems](https://www.irdes.fr/english/reports/586-the-environmental-sustainability-of-health-care-systems.pdf)

### Contexte et ordres de grandeurs

[Pichler et al., 2019] showed that the carbon footprint of healthcare systems
in industrialized countries is of the same order than the agriculture sector. 
In France, the estimated footprint is 8% [Sénéchal, 2023].