# 📰 Blog posts 

Mainly (lazy) review of papers that I found interesting.


```{postlist}
:list-style: circle
:date: "%Y-%m-%d"
:format: "{title} ({date})"
:excerpts:
```
