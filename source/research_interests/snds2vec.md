# Event2vec

Medico-administrative databases are rich sources of information on health systems. However, their 
exploitation is delicate because of their complexity. I hope to take advantage of unsupervised 
approaches that have proven to be very effective in Natural Language Processing by applying the 
**word2vec** method to sequences of care. I worked on vectorial representations that reflect 
the interactions (co-occurrences) during care pathways between the codes or events of four major 
French medical terminologies. This method has already been conducted on mixed data types such as text,
 American health insurance databases and articles published by Beam et al, (2018)[^1]. 

## Python package

I tried to provide simple implementations of these methods in a python package, [event2vec](https://straymat.gitlab.io/event2vec/). 

## Applications

## SNDS, French National Health Insurance database

I applied these methods on the French National Health Insurance database, the [SNDS](https://documentation-snds.health-data-hub.fr/) containing 
billions of claims. For each separate event (medical concept) present in the data, a vector of 
dimension 150 (embedding) is obtained.  

![](/_static/files/images/snds2vec.png)

[A poster summarizing](../_static/files/snds2vec_printable_poster.pdf) this work has been presented at the congress of epidemiology, [Emois](http://emois.org/) : 
Doutreligne et al., 2020 [^2].
  
In addition, I provide a web application exposing the vectors as well as some qualitative 
evaluations (2D projection and proximity requests): [https://straymat.gitlab.io/event2vec/visualizations.html](https://straymat.gitlab.io/event2vec/visualizations.html).
 
## On the APHP data

I also applied these methods on the APHP data, the largest hospital in Europe. I am currently describing these experiences in a [working paper](https://github.com/strayMat/predictive_ehr_paper).

[^1]: [A. L. Beam et al, " Clinical Concept Embeddings Learned from Massive Sources of Multimodal Medical Data ", arXiv:1804.01486[cs, stat], avr. 2018.](http://arxiv.org/abs/1804.01486)
  
[^2]: [M. Doutreligne, A. Leduc, D.-P. Nguyen, et A. Vuagnat, « Snds2vec, représentations continues pour les concepts médicaux du Système national des données de santé », Revue d’Épidémiologie et de Santé Publique, vol. 68, p. S35, mars 2020, doi: 10.1016/j.respe.2020.01.077.](https://www.sciencedirect.com/science/article/abs/pii/S0398762020300894?via%3Dihub)